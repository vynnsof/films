import FilmsList from "pages/FilmsPage/components/FilmsList";
import { createContext } from "react";

const FilmContext = createContext();

export default FilmContext;

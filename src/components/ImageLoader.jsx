const ImageLoader = ({ src, fallbackImg, alt, ...rest }) => {
  const onError = ({ target }) => (target.src = fallbackImg);

  return <img onError={onError} src={src} alt={alt} {...rest} />;
};

export default ImageLoader;

import React, { Component } from "react";
import { prop, sortWith, ascend, descend } from "ramda";
import { generate as id } from "shortid";
import FilmsList from "pages/FilmsPage/components/FilmsList";
import FilmContext from "contexts/FilmContext";
import FilmForm from "pages/FilmsPage/components/FilmForm";
// import LoginForm from 'pages/LoginPage/components/LoginForm'
// import SignupForm from 'pages/SignupPage/components/SignupForm'
import TopNavigation from "components/TopNavigation";
import { films } from "data";

class App extends Component {
  state = {
    films: [],
    showAddForm: false,
    selectedFilm: {},
  };

  componentDidMount() {
    this.setState({ films: this.sortFilms(films) });
  }

  sortFilms = (films) =>
    sortWith([descend(prop("featured")), ascend(prop("title"))], films);

  toggleFeatured = (id) =>
    this.setState(({ films }) => ({
      films: this.sortFilms(
        films.map((f) => (f._id === id ? { ...f, featured: !f.featured } : f))
      ),
    }));

  showForm = (e) => this.setState({ showAddForm: true, selectedFilm: {} });
  hideForm = (e) => this.setState({ showAddForm: false, selectedFilm: {} });

  selectFilmForEdit = (selectedFilm) =>
    this.setState({
      selectedFilm,
      showAddForm: true,
    });

  saveFilm = (film) =>
    this.setState(({ films, showAddForm }) => ({
      films: this.sortFilms([...films, { _id: id(), ...film }]),
      showAddForm: false,
    }));

  value = {
    toggleFeatured: this.toggleFeatured,
    selectFilmForEdit: this.selectFilmForEdit,
  };

  render() {
    const { films, showAddForm, selectedFilm, submit } = this.state;
    const cls = showAddForm ? "ten" : "sixteen";

    return (
      <FilmContext.Provider value={this.value}>
        <div className="ui container mt-3">
          <TopNavigation showForm={this.showForm} />

          <div className="ui stackable grid">
            {/* {showAddForm && (
              <div className="six wide column">
                <FilmForm
                  film={selectedFilm}
                  saveFilm={this.saveFilm}
                  hideForm={this.hideForm}
                />
              </div>
            )} */}

            <div className={`${cls} wide column`}>
              <FilmsList films={films} />
              {/* <SignupForm></SignupForm> */}
            </div>
          </div>
        </div>
      </FilmContext.Provider>
    );
  }
}

export default App;

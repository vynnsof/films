import React, { Component } from "react";
import FormMessage from "components/FormMessage";

const initialData = {
  email: "",
  password: "",
  confirmPassword: "",
};

class SignupForm extends Component {
  state = {
    data: initialData,
    errors: {},
    loading: false,
  };

  handleChange = (e) => {
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value },
      errors: { ...this.state.errors, [e.target.name]: "" },
    });
  };

  validate(data) {
    const errors = {};

    if (!data.email) errors.email = "Email cannot be blank";
    if (!data.password) errors.password = "Password cannot be blank";
    if (!data.confirmPassword) errors.confirmPassword = "Password cannot be blank";

    if ( data.password && data.confirmPassword && data.password !== data.confirmPassword) {
      errors.confirmPassword = "Passwords doesn't match";
    }

    return errors;
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const errors = this.validate(this.state.data);

    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ data: initialData, errors: {} });
    } 
    // else{
    //     this.setState({loading: true})
    // }
  };

  cancelFormBtn = () => {
    this.setState({data: initialData, errors: {}})
  };

  render() {
    const { data, errors, loading } = this.state;
    const cls = loading ? "ui form loading" : "ui form";

    return (
      <form onSubmit={this.handleSubmit} className={cls}>
        <div className="ui centered grid mb-3">
          <div className="six wide column">
            {/* email START */}
            <div className={`field ${errors.email ? "error" : ""}`}>
              <label htmlFor="title">Email</label>
              <input
                value={data.email}
                onChange={this.handleChange}
                type="email"
                name="email"
                id="email"
                placeholder="Email"
              />
              {errors.email && <FormMessage>{errors.email}</FormMessage>}
            </div>
            {/* email END */}

            {/* pass START */}
            <div className={`field ${errors.password ? "error" : ""}`}>
              <label htmlFor="title">Password</label>
              <input
                value={data.password}
                onChange={this.handleChange}
                type="password"
                name="password"
                id="password"
                placeholder="Password"
              />
              {errors.password && <FormMessage>{errors.password}</FormMessage>}
            </div>
            {/* pass END */}

            {/* pass confirm START */}
            <div className={`field ${errors.confirmPassword ? "error" : ""}`}>
              <label htmlFor="title">Confirm password</label>
              <input
                value={data.confirmPassword}
                onChange={this.handleChange}
                type="password"
                name="confirmPassword"
                id="confirmPassword"
                placeholder="Confirm password"
              />
              {errors.confirmPassword && <FormMessage>{errors.confirmPassword}</FormMessage>}
            </div>
            {/* pass confirm END */}

            {/* Buttons START */}
            <div className="ui fluid buttons">
              <button className="ui button primary" type="submit">
                Ok
              </button>
              <div className="or"></div>
              <span onClick={this.cancelFormBtn} className="ui button">
                Cancel
              </span>
            </div>
            {/* Buttons END */}
          </div>
        </div>
      </form>
    );
  }
}

export default SignupForm;

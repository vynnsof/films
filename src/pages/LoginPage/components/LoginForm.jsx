import React, { Component } from "react";
import FormMessage from "components/FormMessage";

const initialData = {
  email: "",
  password: "",
};

class LoginForm extends Component {
  state = {
    data: initialData,
    errors: {},
    loading: false,
  };

  handleChange = (e) => {
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value },
      errors: { ...this.state.errors, [e.target.name]: "" },
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    this.setState({ errors });

    if (Object.keys(errors).length === 0) {
      this.setState({ data: initialData, errors: {} });
    }
    // else{
    //   this.setState({ loading: true });
    // }
  };

  validate(data) {
    const errors = {};
    if (!data.email) errors.email = "Email cannot be blank";
    if (!data.password) errors.password = "Password cannot be blank";

    return errors;
  }

  cancelFormBtn = () => {
    this.setState({ data: initialData, errors: {} });
  };

  render() {
    const { data, errors, loading } = this.state;
    const cls = loading ? "ui form loading" : "ui form";

    return (
      <form onSubmit={this.handleSubmit} className={cls}>
        <div className="ui centered grid mb-3">
          <div className="six wide column">
            {/* email START */}
            <div className={`field ${errors.email ? "error" : ""}`}>
              <label htmlFor="title">Email</label>
              <input
                value={data.email}
                onChange={this.handleChange}
                type="email"
                name="email"
                id="email"
                placeholder="Email"
              />
              {errors.email && <FormMessage>{errors.email}</FormMessage>}
            </div>
            {/* email END */}

            {/* pass START */}
            <div className={`field ${errors.password ? "error" : ""}`}>
              <label htmlFor="title">Password</label>
              <input
                value={data.password}
                onChange={this.handleChange}
                type="password"
                name="password"
                id="password"
                placeholder="Password"
              />
              {errors.password && <FormMessage>{errors.password}</FormMessage>}
            </div>
            {/* pass END */}

            {/* Buttons START */}
            <div className="ui fluid buttons">
              <button className="ui button primary" type="submit">
                Ok
              </button>
              <div className="or"></div>
              <span onClick={this.cancelFormBtn} className="ui button">
                Cancel
              </span>
            </div>
            {/* Buttons END */}
          </div>
        </div>
      </form>
    );
  }
}

export default LoginForm;
